/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.server;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.util.EC2MetadataUtils;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.Executors;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.filter.LogFilter;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.filter.QueryStringFilter;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.server.handler.HealthHandler;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.server.handler.RunnerHandler;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.server.runnables.UpdateTask;

public class Server {

  /**
   * The port the server is running on.
   */
  private static final int PORT = 8000;

  /**
   * HTTP Server instance.
   */
  private static HttpServer server;

  public static void main(String[] args) throws IOException {
    InetSocketAddress address = new InetSocketAddress(PORT);
    Runtime runtime = Runtime.getRuntime();

    // Bootstrap the server
    server = HttpServer.create(address, 0);
    server.setExecutor(Executors.newCachedThreadPool());
    server.createContext("/mzrun.html", new RunnerHandler())
        .getFilters().addAll(Arrays.asList(new LogFilter(), new QueryStringFilter()));
    server.createContext("/health", new HealthHandler());
    server.start();

    registerToLoadBalancer();
    // Add a cleanup shutdown hook
    runtime.addShutdownHook(new Thread() {
      @Override
      public void run() {
        cleanup();
      }
    });
  }

  private static void cleanup() {
    server.stop(0);
  }

  private static String getLoadBalancerEndpoint() {
    DynamoDB dynamo = new DynamoDB(AmazonDynamoDBClientBuilder.standard().withRegion(
        Regions.EU_WEST_2).withCredentials(new ProfileCredentialsProvider()).build());
    Table t = dynamo.getTable(UpdateTask.TABLE_NAME);
    GetItemSpec spec = new GetItemSpec().withPrimaryKey("instance_id", "load_balancer");
    Item i = t.getItem(spec);
    String name = "";
    if(i != null) { //load balancer is registered in Dynamo
      name = i.getString("name");
    }

    return name;
  }

  private static void registerToLoadBalancer() {
    String name = getLoadBalancerEndpoint();
    if(name.equals("")) {
      System.out.println("No Load Balancer endpoint found");
      //TODO: no clue how to proceed here
    }
    else {
      try {
        String endpoint = "http://" + name + "/register_instance";
        String instanceId = EC2MetadataUtils.getInstanceId();
        URL url = new URL(endpoint);
        HttpURLConnection urlConnection = ((HttpURLConnection) url.openConnection());
        urlConnection.setRequestMethod("POST");
        urlConnection.setDoOutput(true);
        String message = Metrics.getInstanceName() + ":" + PORT + ";" + (instanceId == null ? "localhost" : instanceId);
        urlConnection.getOutputStream().write(message.getBytes());
        urlConnection.getInputStream();
        System.out.println("Registered in the Load Balancer");
      } catch (IOException e) {
        System.out.println(e.getMessage());
        e.printStackTrace();
      }
    }
  }
}
