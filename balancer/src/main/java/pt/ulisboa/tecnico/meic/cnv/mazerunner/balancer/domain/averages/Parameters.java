/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.averages;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.ThreadMetrics;

public class Parameters {

  public static final int PERCENTILE_20 = 20;
  public static final int PERCENTILE_40 = 40;
  public static final int PERCENTILE_60 = 60;
  public static final int PERCENTILE_80 = 80;
  public static final int PERCENTILE_100 = 100;

  private String strategy;
  private String maze;
  private int distance;

  public Parameters(String strategy, String maze, int distance) {
    this.strategy = strategy;
    this.maze = maze;
    this.distance = distance;
  }

  public Parameters(ThreadMetrics metrics) {
    this.strategy = metrics.getStrategy();
    this.maze = metrics.getMaze();
    this.distance = getDistanceDelta(metrics.getDistance(), Integer.valueOf(maze.substring(4)));
  }

  private int getDistanceDelta(int distance, int mazeSize) {
      long maxDistance = Math.round(Math.sqrt(((mazeSize)^2) * 2));
      int percentage = Math.round((distance / maxDistance) * 100);

      List<Integer> list = Arrays.asList(PERCENTILE_20, PERCENTILE_40, PERCENTILE_60, PERCENTILE_80, PERCENTILE_100);
      return list.stream().min(Comparator.comparingInt(i -> (percentage > i ? 1 : -1))).get();
  }

  public String getStrategy() {
    return strategy;
  }

  public String getMaze() {
    return maze;
  }

  public int getDistance() {
    return distance;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Parameters that = (Parameters) o;
    return distance == that.distance &&
        Objects.equals(strategy, that.strategy) &&
        Objects.equals(maze, that.maze);
  }

  @Override
  public int hashCode() {

    return Objects.hash(strategy, maze, distance);
  }
}
