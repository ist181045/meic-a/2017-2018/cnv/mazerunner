/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;

public class ThreadMetrics {

  private String strategy;
  private long methodCalls;
  private int distance;
  private int speed;
  private String maze;

  public ThreadMetrics(String strategy, long methodCalls, String maze, int distance, int speed) {
    this.strategy = strategy;
    this.methodCalls = methodCalls;
    this.distance = distance;
    this.speed = speed;
    this.maze = maze;
  }

  @DynamoDBAttribute
  public String getStrategy() {
    return strategy;
  }

  @DynamoDBAttribute
  public long getMethodCalls() {
    return methodCalls;
  }

  @DynamoDBAttribute
  public int getDistance() {
    return distance;
  }

  @DynamoDBAttribute
  public int getSpeed() {
    return speed;
  }

  @DynamoDBAttribute
  public String getMaze() {
    return maze;
  }

}
