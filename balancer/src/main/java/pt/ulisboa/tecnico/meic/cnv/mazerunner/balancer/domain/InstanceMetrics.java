/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InstanceMetrics {

  private static final int MAX_ATTEMPTS = 2;

  private String key;
  private String port;
  private String instanceId;
  private Map<String, ThreadMetrics> currentThreads = new ConcurrentHashMap<>();
  private int failedAttempts = 0;
  private boolean healthy = true;
  private boolean toBeRemoved = false;

  public InstanceMetrics(String key, String instanceId, String port) {
    this.key = key;
    this.instanceId = instanceId;
    this.port = port;
  }

  public Map<String, ThreadMetrics> getCurrentThreads() {
    return currentThreads;
  }

  public void setCurrentThreads(
      Map<String, ThreadMetrics> currentThreads) {
    this.currentThreads = currentThreads;
  }

  public int getFailedAttempts() {
    return failedAttempts;
  }

  public void addFailedAttempt() {
    if(++failedAttempts == MAX_ATTEMPTS && isHealthy()) { //consider it to be dead eventually?
      setHealthy(false);
      System.out.println("Instance " + key + " is unhealthy");
    }
  }

  public void addSuccessfulAttempt() {
    if(--failedAttempts == -MAX_ATTEMPTS && !isHealthy()) {
      setHealthy(true);
      System.out.println("Instance " + key + " is healthy");
    }
  }


  public boolean isHealthy() {
    return healthy;
  }

  public void setHealthy(boolean healthy) {
    this.healthy = healthy;
  }

  public boolean isToBeRemoved() {
    return toBeRemoved;
  }

  public void setToBeRemoved(boolean toBeRemoved) {
    this.toBeRemoved = toBeRemoved;
  }

  public String getInstanceId() {
    return instanceId;
  }

  public String getPort() {
    return port;
  }
}
