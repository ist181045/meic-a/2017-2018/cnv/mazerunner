/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.runnables;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.RebootInstancesRequest;
import com.amazonaws.services.ec2.model.RebootInstancesResult;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;
import java.sql.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.TimerTask;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.InstanceManager;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.InstanceMetrics;

public class AutoscaleTask extends TimerTask {

  private static final String IMAGE_ID = "ami-001d99d1a713b4f8f";
  private static final String INSTANCE_TYPE = "t2.micro";
  private static final String KEY_PAIR_NAME = "cnv";
  private static final String SECURITY_GROUP = "cnv-sg-mazerunner";
  private static final long UPDATE_TIME = 1000 * 60 * 5; //5 minutes

  private static final int UPPER_BOUND = 80;
  private static final int LOWER_BOUND = 60;
  private static final int THREADS_PER_INSTANCE = 2;

  private AmazonEC2 client = AmazonEC2ClientBuilder.standard().withRegion(Regions.EU_WEST_2)
      .withCredentials(new ProfileCredentialsProvider()).build();

  private AmazonCloudWatch cwClient = AmazonCloudWatchClientBuilder.standard()
      .withRegion(Regions.EU_WEST_2).withCredentials(new ProfileCredentialsProvider()).build();


  @Override
  public void run() {
    InstanceManager instance = InstanceManager.getInstance();

    Date currentDate = new Date(System.currentTimeMillis());

    int nInstances = 0;
    int total = 0;
    int nThreads = 0;

    System.out.println();
    for (Entry<String, InstanceMetrics> element : instance.getCurrentInstances().entrySet()) {
      if(element.getValue().getInstanceId().equals("localhost")) { //for local testing purposes no need to query CW
        continue;
      }

      if(element.getValue().isToBeRemoved()) {
        if(element.getValue().getCurrentThreads().size() == 0) {
          removeInstance(element.getValue().getInstanceId(), element.getKey());
          continue;
        }
        else {
          continue;
        }
      }

      if ((currentDate.getTime() - instance.getLastUpdate().getTime()) <= UPDATE_TIME) {
        continue;
      }

      nInstances++;
      nThreads += element.getValue().getCurrentThreads().size();
      GetMetricStatisticsRequest request = new GetMetricStatisticsRequest()
          .withNamespace("AWS/EC2")
          .withStartTime(new Date(System.currentTimeMillis() - UPDATE_TIME))
          .withEndTime(new Date(System.currentTimeMillis())).withPeriod(60)
          .withMetricName("CPUUtilization").withDimensions(
              new Dimension().withName("InstanceId").withValue(element.getValue().getInstanceId()))
          .withStatistics("Average");

      GetMetricStatisticsResult result;
      try {
        result = cwClient.getMetricStatistics(request);
      } catch (Exception e) {
        e.printStackTrace();
        return;
      }
      int datapoints = 0;
      int instanceTotal = 0;
      for (Datapoint datapoint : result.getDatapoints()) {
        datapoints++;
        instanceTotal += datapoint.getAverage();
      }
      total += (datapoints == 0 ? 0 :(instanceTotal / datapoints));
    }

    if(nInstances == 0 || total == 0) {
      return;
    }

    if((total / nInstances) >= UPPER_BOUND && nThreads >= (nInstances * THREADS_PER_INSTANCE)) {
      System.out.println(String.format("[%tT] Attempting to start a new instance", System.currentTimeMillis()));
      List<String> stoppedInstances = instance.getStoppedInstances();
      List<String> toBeRemoved = instance.getToBeRemoved();
      if(toBeRemoved.size() > 0) { //if there are instances marked for removal they stop being like that
        instance.getCurrentInstances().get(toBeRemoved.get(0)).setToBeRemoved(false);
        instance.getToBeRemoved().remove(0);
      }
      else if(stoppedInstances.size() > 0) { //if instances have been previously stopped use those instead of launching a new one
        rebootInstance(stoppedInstances.get(0));
        instance.getStoppedInstances().remove(0);
      }
      else { //launch a new instance
        addInstance();
      }
      instance.setLastUpdate(new Date(System.currentTimeMillis()));
    }
    else if((total / nInstances) <= LOWER_BOUND && nInstances > 1) {
      instance.markInstanceForRemoval();
      instance.setLastUpdate(new Date(System.currentTimeMillis()));
    }
  }

  private void removeInstance(String instanceId, String instanceName) {
    try {
      StopInstancesRequest request = new StopInstancesRequest().withInstanceIds(instanceId);
      StopInstancesResult result = client.stopInstances(request);
      InstanceManager.getInstance().removeInstance(instanceName);
    } catch (Exception e) {
      e.printStackTrace();
      //TODO: idk...
    }
  }

  private void addInstance() {
    try {
      RunInstancesRequest request = new RunInstancesRequest().withImageId(IMAGE_ID)
          .withInstanceType(INSTANCE_TYPE).withMinCount(1).withMaxCount(1).withKeyName(KEY_PAIR_NAME)
          .withSecurityGroups(SECURITY_GROUP);
      RunInstancesResult result = client.runInstances(request);
      System.out.println(String.format("[%tT] Started instance with id %s", System.currentTimeMillis(), result.getReservation().getInstances().get(0).getInstanceId()));
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  private void rebootInstance(String instanceId) {
    try {
      RebootInstancesRequest request = new RebootInstancesRequest().withInstanceIds(instanceId);
      RebootInstancesResult result = client.rebootInstances(request);
      System.out.println(String.format("[%tT] Rebooted instance with id %s", System.currentTimeMillis(), instanceId));
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

}
