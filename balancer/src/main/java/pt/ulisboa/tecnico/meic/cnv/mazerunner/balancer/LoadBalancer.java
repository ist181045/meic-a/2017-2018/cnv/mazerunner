/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.util.EC2MetadataUtils;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.handler.MazeHandler;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.handler.RegisterHandler;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.runnables.GetUpdateTask;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.filter.QueryStringFilter;


public class LoadBalancer
{

    /**
     * The port the server is running on.
     */
    private static final int PORT = 80;

    /**
     * HTTP Server instance.
     */
    private static HttpServer server;

    public static void main(String[] args) throws IOException {
        InetSocketAddress address = new InetSocketAddress(PORT);
        Runtime runtime = Runtime.getRuntime();

        // Bootstrap the server
        server = HttpServer.create(address, 0);
        server.setExecutor(Executors.newCachedThreadPool());
        server.createContext("/register_instance", new RegisterHandler());
        server.createContext("/mzrun.html", new MazeHandler()).getFilters().add(new QueryStringFilter());
        server.start();
        register();

        // Add a cleanup shutdown hook
        runtime.addShutdownHook(new Thread() {
            @Override
            public void run() {
                cleanup();
            }
        });
    }

    private static void cleanup() {
        server.stop(0);
    }

    private static void register() {
      try {
        DynamoDB dynamo = new DynamoDB(AmazonDynamoDBClientBuilder.standard().withRegion(
            Regions.EU_WEST_2).withCredentials(new ProfileCredentialsProvider()).build());
        String name = (EC2MetadataUtils.getLocalHostName() != null ? EC2MetadataUtils.getLocalHostName() : "194.210.230.210") + ":" + PORT;
        Table t = dynamo.getTable(GetUpdateTask.TABLE_NAME);
        Item i = new Item().withPrimaryKey("instance_id", "load_balancer").withString("name", name);
        t.putItem(i);
        System.out.println("LoadBalancer registered its name in DynamoDB: " + name);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
}
