/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.runnables;

import java.net.URI;
import java.sql.Date;
import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;
import jdk.incubator.http.HttpResponse.BodyHandler;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.InstanceManager;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.InstanceMetrics;

public class HealthCheckRunnable implements Runnable {

  private static final int MAX_DELAY = 5 * 1000; //5 seconds max delay between request and response
  private static final String HEALTH_ENDPOINT = "/health";

  private String instanceName;
  private String instancePort;

  HealthCheckRunnable(String instanceName, String port) {
    this.instanceName = instanceName;
    this.instancePort = port;
  }

  @Override
  public void run() {
    InstanceManager instance = InstanceManager.getInstance();
    HttpClient client = HttpClient.newHttpClient();

    String endpoint = "http://" + instanceName + ":" + instancePort + HEALTH_ENDPOINT;

    InstanceMetrics instanceMetrics = instance.getCurrentInstances().get(instanceName);
    if(instanceMetrics == null) {
      return;
    }
    try {
      Date before = new Date(System.currentTimeMillis());
      HttpResponse<String> response = client.send(
          HttpRequest.newBuilder(new URI(endpoint))
              .GET()
              .build(), BodyHandler.asString());
      Date after = new Date(System.currentTimeMillis());

      long interval = after.getTime() - before.getTime();

      if(interval > MAX_DELAY) {
        instanceMetrics.addFailedAttempt();
      }
      else {
        instanceMetrics.addSuccessfulAttempt();
      }

    } catch (Exception e) {
      instanceMetrics.addFailedAttempt();
    }

  }

}
