/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.net.ssl.SSLParameters;
import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpClient.Version;
import jdk.incubator.http.HttpHeaders;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;
import jdk.incubator.http.HttpResponse.BodyHandler;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.InstanceManager;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.balancer.domain.ThreadMetrics;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.filter.QueryStringFilter;
import pt.ulisboa.tecnico.meic.cnv.mazerunner.util.Exchanges;

public class MazeHandler implements HttpHandler {


  private static final List<String> MAZE_ARGS = List.of("m", "x0", "y0", "x1", "y1", "v", "s");


  @Override
  public void handle(HttpExchange exchange) throws IOException {

    System.out.printf("[%tT] Received request from %s%n", new Date(System.currentTimeMillis()),
            exchange.getRemoteAddress());

    InstanceManager manager = InstanceManager.getInstance();

    @SuppressWarnings("unchecked")
    var query = (Map<String, String>) exchange.getAttribute(QueryStringFilter.ATTRIBUTE_QUERY_MAP);

    String instanceName;
    ThreadMetrics metrics;
    if (query.keySet().containsAll(MAZE_ARGS)) {
      var maze = query.get("m").substring(0, query.get("m").indexOf("."));
      var strategy = query.get("s");
      var velocity = Integer.valueOf(query.get("v"));
      var dx = Double.valueOf(query.get("x1")) - Double.valueOf(query.get("x0"));
      var dy = Double.valueOf(query.get("y1")) - Double.valueOf(query.get("y0"));
      var distance = (int) Math.round(Math.hypot(dx, dy));

      metrics = new ThreadMetrics(strategy, 0, maze, distance, velocity);
      instanceName = manager.decideNextRequest(metrics);
    } else {
      Exchanges.sendError(HttpURLConnection.HTTP_BAD_REQUEST, exchange, "Missing arguments: "
          + String.join(", ", MAZE_ARGS.stream()
          .filter(a -> !query.containsKey(a))
          .collect(Collectors.toList())));
      return;
    }

    if (instanceName == null) {
      Exchanges.sendResponse(HttpURLConnection.HTTP_INTERNAL_ERROR, exchange,
          "No healthy instances available");
      return;
    }

    System.out.printf("[%tT] Forwarded request to %s%n",
        new Date(System.currentTimeMillis()), instanceName);
    HttpResponse<String> response = forwardRequest(
        "http://" + instanceName + exchange.getRequestURI());
    while (response == null) {
      manager.getCurrentInstances()
          .get(instanceName.substring(0, instanceName.indexOf(":")))
          .setHealthy(false); //not sure if this is enough to ensure no more requests
      String oldInstance = instanceName;
      instanceName = manager.decideNextRequest(metrics);
      if (instanceName == null) {
        Exchanges.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR, exchange,
            "No healthy instances available");
        break;
      }
      System.out.printf("[%tT] Instance %s did not respond, attempting to forward the request to "
          + "%s%n", System.currentTimeMillis(), oldInstance, instanceName);
      response = forwardRequest("http://" + instanceName + exchange.getRequestURI());
    }
    if (response != null) {
      Exchanges.sendResponse(response.statusCode(), exchange, response.body());
    }

    System.out.printf("[%tT] Sent reply to %s%n", new Date(System.currentTimeMillis()),
        exchange.getRemoteAddress());
  }

  private HttpResponse<String> forwardRequest(String endpoint) {
    HttpClient client = HttpClient.newHttpClient();
    try {
      return client.send(
          HttpRequest.newBuilder(new URI(endpoint))
              .GET()
              .build(), BodyHandler.asString());
    } catch (IOException | InterruptedException e) {
      return null;
    } catch (URISyntaxException e) {
      return new HttpResponse<>() {
        @Override
        public int statusCode() {
          return 400;
        }

        @Override
        public HttpRequest request() {
          return null;
        }

        @Override
        public Optional<HttpResponse<String>> previousResponse() {
          return Optional.empty();
        }

        @Override
        public HttpHeaders headers() {
          return null;
        }

        @Override
        public String body() {
          return "Invalid request";
        }

        @Override
        public SSLParameters sslParameters() {
          return null;
        }

        @Override
        public URI uri() {
          return null;
        }

        @Override
        public Version version() {
          return null;
        }
      };
    }
  }
}
