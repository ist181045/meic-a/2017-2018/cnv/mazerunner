/*
 * MIT License
 *
 * Copyright (c) 2018 Pedro Cerejo, João Oliveira, Rui Ventura
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pt.ulisboa.tecnico.meic.cnv.mazerunner.util;

import java.util.HashMap;
import java.util.Map;

public class Uris {

  private Uris() {
  }

  public static Map<String, String> parseQuery(String query) throws IllegalArgumentException {
    if (query == null) {
      throw new IllegalArgumentException("Empty query string");
    }

    Map<String, String> map = new HashMap<>();
    String[] pairs = query.split("&");

    if (pairs.length == 0) {
      throw new IllegalArgumentException("Bad query string");
    }

    for (String pair : pairs) {
      String[] keyValue = pair.split("=");
      if (keyValue.length != 2) {
        throw new IllegalArgumentException("Bad query string pair");
      }
      map.put(keyValue[0], keyValue[1]);
    }

    return map;
  }
}
